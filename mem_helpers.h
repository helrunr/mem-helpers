#ifndef MEM_HELPERS_MEM_HELPERS_H
#define MEM_HELPERS_MEM_HELPERS_H

void *allocate_tracker(int size);
void *deallocate_tracker(void *handler);
int mem_leak_tracker(void);

#endif //MEM_HELPERS_MEM_HELPERS_H
