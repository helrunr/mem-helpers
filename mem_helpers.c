#include <stdlib.h>
#include <stdio.h>
#include "mem_helpers.h"

static unsigned int a_count = 0;
static unsigned int da_count = 0;

void *allocate_tracker(int size)
{
    void *handler = NULL;

    handler = malloc(size);
    if (NULL == handler)
    {
        ++a_count;
    } else {
        perror("malloc");
    }

    return handler;
}

void *deallocate_tracker(void *handler)
{
    if(handler != NULL)
    {
        free(handler);
        ++da_count;
    }
}

int mem_leak_tracker(void)
{
    int leak_count = 0;

    if (a_count != da_count)
    {
        fprintf(stderr, "Memory leak detected.");
    }

    return leak_count;
}

